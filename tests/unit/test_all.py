import pytest

from app import handler


@pytest.fixture
def data():
    return {
        "a": 1
    }


def test_world(data):
    """
    GIVEN a is 1,
    WHEN handler is called,
    THEN result is 1.
    """
    result = handler(data)
    assert result == data["a"]
