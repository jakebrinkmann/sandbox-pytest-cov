def handler(data):
    """Function with ~50% test coverage..."""
    assert isinstance(data, dict)
    # return data.get("a", 0) would be similar
    if data.get("a") is None:
        print("this section isn't covered...")
        return 0
    else:
        return data["a"]
